import Banner from '../components/Banner';
import { Link } from 'react-router-dom';
import {Button} from 'react-bootstrap';

export default function PageNotFound() {

  const data = {
        title: "404 - Not found",
        content: "The page you are looking for cannot be found",
        destination: "/",
        label: "Back home"
    }



  return (
    <Banner data={data}/>
  );
}
