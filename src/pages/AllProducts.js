




import {Fragment, useEffect, useState, useContext} from 'react';
import {Container} from 'react-bootstrap';
import ViewAllProduct from '../components/ViewAllProduct';
import UserContext from '../UserContext';


export default function AllProducts() {

	const { user } = useContext(UserContext);


	const[viewProducts, setViewProducts] = useState([])

	useEffect(() => {
		fetch('https://fathomless-spire-69300.herokuapp.com/products/allproducts', {
			headers: {
				"Content-type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setViewProducts(data.map(viewProducts => {

				return(
					<ViewAllProduct key={viewProducts._id} viewAllProp={viewProducts} />
					)
			}))
		})


	}, [])


	return(
		
		<Container className="container-fluid">
			<h2 className="text-center p-3">Products</h2>
		<div className="row">
		<div className="col-xs">
		<div className="box">
		<div className="col-xs">
			<div id="admin-view">{viewProducts}</div>
		</div>
		</div>
		</div>
		</div>

		</Container>
		
			
		
		

		)


}