import {useState, useEffect, useContext} from 'react';
import {Button, Col, Row, FloatingLabel, Form, Container} from 'react-bootstrap';
import {useParams} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';



export default function ProductUpdate() {

	const { user } = useContext(UserContext);
	const{productId} = useParams()

	const [productImage, setProductImage] = useState("");
	const [productName, setProductName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");

	const updateProduct = (e) => {

		fetch(`https://fathomless-spire-69300.herokuapp.com/products/update/${productId}`, {
			method: "PUT",
			headers: {
				"Content-type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
					productImage: productImage,
					productName: productName,
					description: description,
					price: price

				})
		})
		.then(res => res.json())
			.then(data => {
				console.log(data)

				if(data === true) {

						Swal.fire({
								title: "Updated Successfully",
								icon: "success",
								text: "You may now log in."
							})

				} else {
						Swal.fire({
								title: "Something went wrong",
								icon: "error",
								text: "Please try again"
							})
				}

			})


	}


	useEffect(() => {
		

		fetch(`https://fathomless-spire-69300.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setProductName(data.productName)
			setDescription(data.description)
			setPrice(data.price)


		})

	}, [productId])



	return(

		<Container >
			  <Form onSubmit={(e) => updateProduct(e)}>
			    <Row className="g-2 mt-3">
			      <Col lg={{span:8, offset: 2}} >
			      <h3>Update Product</h3>

			      
			      	<FloatingLabel controlId="productImage" label="Product Image">
			      	  <Form.Control width="300" type="text"  onChange={e => {
							  			setProductImage(e.target.value)
							  		}} placeholder="Product Image" />
			      	</FloatingLabel>

			        
			        <FloatingLabel controlId="productName" label="Product Name">
			          <Form.Control size="lg" type="text" 
							  		placeholder="Enterr Product Name"
							  		value={productName}
							  		onChange={e => {
							  			setProductName(e.target.value)
							  		}}
							  		required  />
			        </FloatingLabel>

			       	  
			          <FloatingLabel controlId="description" label="Description">
			            <Form.Control size="lg" type="text" 
							  		placeholder="Enter Description"
							  		value={description}
							  		onChange={e => {
							  			setDescription(e.target.value)
							  		}}
							  		required  />
			          </FloatingLabel>


			        
			          <FloatingLabel controlId="price" label="price">
			            <Form.Control size="lg" type="text" 
							  		placeholder="Enter Price"
							  		value={price}
							  		onChange={e => {
							  			setPrice(e.target.value)
							  		}}
							  		required   />
			          </FloatingLabel>

			          <Button id="admin-btn" type="submit" >
		   				  Update
		   				</Button>


			      </Col>
			     </Row>
			 </Form>
		  </Container>



		)






}


