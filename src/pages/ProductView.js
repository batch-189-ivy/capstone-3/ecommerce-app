import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import { MDBIcon } from 'mdb-react-ui-kit';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ProductView() {


	const [quantity, setQuantity] = useState(0)

	function addQuantity() {
		if (quantity !== 20 ) {
			setQuantity(quantity + 1)
			
		}
	}

	function subtractQuantity() {
		if (quantity !== 0) {
			setQuantity(quantity - 1)
			
		}
	}


	const { user } = useContext(UserContext);

	//Redirect a user to a different pagee after eeroll
	const navigate = useNavigate()

	//The "useParams"
	const{productId} = useParams()
	

	const [productImage, setProductImage] = useState("");
	const [productName, setProductName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)
	const [subTotal, setSubtotal] = useState(0)


	const addToCart = () => {

		 	fetch(`https://fathomless-spire-69300.herokuapp.com/users/addtocart/`, {
			method:"POST",
			headers: {
				"Content-type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				productImage: productImage,
				productName: productName,
				quantity: quantity,
				price: price,
				subTotal: subTotal
			
				
			})

		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (data === true) {
				Swal.fire({
					title: "Added items to cart!",
					icon: "success"
					
				})

				navigate("/products")

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again!"
				})
			}

		})
	}



	useEffect(() => {
		

		fetch(`https://fathomless-spire-69300.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)


			setProductImage(data.productImage)
			setProductName(data.productName)
			setDescription(data.description)
			setPrice(data.price)
			setSubtotal(data.subTotal)


		})

	}, [productId])


	return(
		<Container id="prod-view" className="pt-5 pb-5 container-fluid">
			<Row>
				<Col lg={{span:6, offset:3}}> 
					<Card>
		            	<Card.Body className="text-center">
		            		<img width="300" alt="product" src={productImage}/>
			                <Card.Title>{productName}</Card.Title>
			                
			                <Card.Subtitle>Description:</Card.Subtitle>
			                <Card.Text>{description}</Card.Text>
			                <Card.Subtitle>Price:</Card.Subtitle>
			                <Card.Text>PhP {price}</Card.Text>
			                <Card.Subtitle><Button id="minus-button" onClick={() => subtractQuantity()}> - </Button><span id="quantity" >{quantity}</span><Button id="add-button" onClick={() => addQuantity()} > + </Button> 
			               
			                </Card.Subtitle>
			                <Card.Subtitle className="pt-3">Subtotal:</Card.Subtitle>
			                <Card.Text>PhP {price * quantity}</Card.Text>
			                
			                
			                {
			                	user.id !== null 

			                	?
			                	<>
			                	{
			                		quantity !== 0
			                		? <Button className="mb-3" id="add-to-cart" onClick={() => addToCart()} >Add to cart</Button>
			                		: <Button disabled className="mb-3" id="add-to-cart" onClick={() => addToCart()} >Add to cart</Button>
			                	}
			                	<Button className="mb-3" id="btn-cart" as={Link} to="/users/mycart"><MDBIcon fas icon="shopping-cart" /></Button>
			                	</>
			                	:

			                	<Link className="btn btn-danger mb-3" to="/login"> Log in </Link>
			                }
		                </Card.Body>
		              </Card>
				</Col>
			</Row>
		</Container>

		)



}
