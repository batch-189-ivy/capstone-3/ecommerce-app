
import {Button, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';

import AdminBanner from '../components/AdminBanner';

export default function Admin() {
	return(

		<>
		  	<AdminBanner/>
		  	<Container className="d-flex justify-content-center">
		  	<Button id="admin-btn" as={Link} to="/allusers">User Database</Button>
		    <Button id="admin-btn" as={Link} to="/createproducts">Create a Product</Button>
		    <Button id="admin-btn" as={Link} to="/allproducts">View All Products</Button>
			</Container>           	
			                	
		</>
	)
}
