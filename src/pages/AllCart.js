/*
import {Fragment, useEffect, useState, useContext} from 'react';
import {Container} from 'react-bootstrap';
import ShowCart from '../components/ShowCart';
import UserContext from '../UserContext';

export default function AllCart() {

	const { user } = useContext(UserContext);

	
	const [allCart, setAllCart] = useState([])

	useEffect(() => {
		

		fetch('https://fathomless-spire-69300.herokuapp.com/users/allorders', {
			headers: {
				"Content-type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setAllCart(data.map(orders => {
					
				return(
					
					<ShowCart key={orders._id} showCartProp={orders} />

					)

			}))
		})

	}, [])


	return(

		<Fragment>
		
		<Container className="p-3 d-inline-block justify-content-center text-center"> 
			<h2 className="p-1">All shopping carts</h2>
			<div>
				<div>
				{allCart}
				</div>
			</div>
		</Container>
		
		</Fragment>



		)
	


}




*/