import { useState, useEffect, useContext } from 'react';
import {Form, Button, Container, FloatingLabel, Col} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");

	
	const[isActive, setIsActive] = useState(false);

	
	function registerUser(e) {
			e.preventDefault();

			fetch("https://fathomless-spire-69300.herokuapp.com/users/checkEmail", {
				method: "POST",
				headers: {
					'Content-type': 'application/json'
				},
				body: JSON.stringify({
					email: email
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				if (data === true) {
					Swal.fire({
						title: "Duplicate Email Found",
						icon: "error",
						text: "Kindly provide another email"
					})

				} else {
					fetch("https://fathomless-spire-69300.herokuapp.com/users/register", {
						method: "POST",
						headers: {
							'Content-Type': "application/json"
						},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							email: email,
							mobileNo: mobileNo,
							password: password1
						})
					})
					.then(res => res.json())
					.then(data => {

						console.log(data)

						if(data === true) {

							Swal.fire({
								title: "Registration Successful",
								icon: "success",
								text: "You may now log in."
							})

							setFirstName("");
							setLastName("");
							setEmail("");
							setMobileNo("");
							setPassword1("");
							setPassword2("");

							navigate("/login");

						} else {
							Swal.fire({
								title: "Something went wrong",
								icon: "error",
								text: "Please try again"
							})
						}


					})
				} 

			})

			
			//alert("You are now registered!");
		}


		useEffect(() => {
			if(( firstName !=="" && lastName !== "" && mobileNo.length === 11 && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)) {
				setIsActive(true)
			} else {
				setIsActive(false)
			} 

		}, [firstName, lastName, email, mobileNo, password1, password2])




	return(
		(user.id !== null) ?
		(user.isAdmin === false) ?
		        <Navigate to="/products" />

		        :

		        <Navigate to="/admin" />

			    :

		

		<Container className="mt-3" >
		 <Form onSubmit={(e) => registerUser(e)}>
		  <Col lg={{span:4, offset:4}} >
		    <h1 className="text-center">Register</h1>

		    <FloatingLabel controlId="firstName" label="First Name" className="mb-3">
				  <Form.Control size="lg" type="text" 
			  		placeholder="Enter First Name"
			  		value={firstName}
			  		onChange={e => {
			  			setFirstName(e.target.value)
			  		}}
			  		required 
			  		/>
		    </FloatingLabel>

		    <FloatingLabel controlId="lastName" label="Last Name" className="mb-3">
				  <Form.Control size="lg" type="text" 
			  		placeholder="Enter Last Name"
			  		value={lastName}
			  		onChange={e => {
			  			setLastName(e.target.value)
			  		}}
			  		required 
			  		/>
		    </FloatingLabel>

		    <FloatingLabel controlId="userEmail" label="Email address" className="mb-3">
				  <Form.Control size="lg" type="email" 
		        		placeholder="Enter email"
		        		value={email}
		        		onChange={e => {
		        			setEmail(e.target.value)
		        		}}
		        		required />
		          <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		          </Form.Text>
		    </FloatingLabel>

		    <FloatingLabel controlId="mobileNo" label="Mobile Number" className="mb-3">
		            <Form.Control size="lg" type="text" 
			  		placeholder="Mobile Number"
			  		value={mobileNo}
			  		onChange={e => {
			  			setMobileNo(e.target.value)
			  		}}
			  		required />
			  		<Form.Text className="text-muted">
		          	Please enter your 11-digit mobile number.
		          </Form.Text>
		   	</FloatingLabel>

		    <FloatingLabel controlId="password1" label="Password" className="mb-3">
		            <Form.Control size="lg" type="password" 
		        		placeholder="Password" 
		        		value={password1}
		        		onChange={e => setPassword1(e.target.value)}
		        		required  />
		   	</FloatingLabel>

		   	 <FloatingLabel controlId="password2" label="Verify Password" className="mb-3">
		   	         <Form.Control size="lg" type="password" 
		   	     		placeholder="Password" 
		   	     		value={password2}
		   	     		onChange={e => setPassword2(e.target.value)}
		   	     		required  />
		   		</FloatingLabel>
		   		{
		   			isActive ?
		   				<Button variant="primary" type="submit" id="submitBtn">
		   				  Submit
		   				</Button>
		   				:
		   				<Button variant="danger" type="submit" id="submitBtn" disabled>
		   				  Submit
		   				</Button>
		   		}

		   </Col>
		  </Form>
		</Container>
		)

}