
import {Fragment, useEffect, useState} from 'react';
import {Container} from 'react-bootstrap';


import UsersCard from '../components/UsersCard';




export default function Users() {



	const [ oneUser, setOneUser] = useState("")



	useEffect(() => {
		

		fetch(`https://fathomless-spire-69300.herokuapp.com/users/details`, {
			headers: {
				"Content-type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setOneUser([data].map(user => {

				return(
				<>
					<UsersCard key={user._id} usersProp={user} />

				</>
					)


			}))

		})

	}, [])





	return(

		<Fragment> 
			<Container className="d-inline-block justify-content-center text-center">
			<h2>MyProfile</h2>
			{oneUser}
			</Container>
		</Fragment>



		)



}


