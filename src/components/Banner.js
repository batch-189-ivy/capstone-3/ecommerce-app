import {Row, Col, Container} from 'react-bootstrap';
import Carousel from 'react-bootstrap/Carousel';


export default function Banner () {

	return (

		<Container id="banner" className="d-flex justify-content-center">
			<Row> 
				<Col className="p-3" lg={{span:8, offset: 2}}> 
					<h1>Liquor Store</h1>
					<p> Drinks for everyone, anytime, anywhere. </p>
					<Carousel variant="dark">
					      <Carousel.Item>
					        <img 
					          className="d-block w-100"
					          src="https://newsd.in/wp-content/uploads/2020/12/66049893_ml.jpg"
					          alt="First slide"
					        />
					        
					      </Carousel.Item>
					      <Carousel.Item>
					        <img id="carousel-2"
					          className="d-block w-100"
					          src="https://cdn.vox-cdn.com/thumbor/IKmOrd6e0o4yFSJOjiTGCPTkXYQ=/0x0:1300x853/1200x0/filters:focal(0x0:1300x853):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/5934593/shutterstock_336565397.0.jpg"
					          alt="Second slide"
					        />
					        
					      </Carousel.Item>
					      <Carousel.Item>
					        <img
					          className="d-block w-100"
					          src="https://cdn.vox-cdn.com/thumbor/gahm_CsvhUmkQzir5bSrvtdPhiM=/cdn.vox-cdn.com/uploads/chorus_asset/file/5934627/shutterstock_256405357.0.jpg"
					          alt="Third slide"
					        />
					        
					      </Carousel.Item>
					    </Carousel>
					
				</Col>
			</Row>
		</Container>


		)



}


