/*
import {Card, Button, Container, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';



export default function ShowCart({showCartProp}) {

	
	
	const {userId, productImage, productName, quantity, price, subTotal} = showCartProp;

    return (

    	<Container className="container-fluid"> 
    	    <Col lg={{span:8, offset: 2}} className="d-flex justify-content-center text-center">
    	        <Card>
    	            <Card.Body>
    	            	<Card.Title>{userId}</Card.Title>

    	                <img width="300" alt="product" src={productImage}/>
    	                <Card.Title>{productName}</Card.Title>
    	                <Card.Subtitle>Price:</Card.Subtitle>
    	                <Card.Text>{price}</Card.Text>
    	                <Card.Subtitle>Quantity:</Card.Subtitle>
    	                <Card.Text>{quantity}</Card.Text>
    	                <Card.Subtitle className="pt-3">Subtotal:</Card.Subtitle>
    	                <Card.Text>PhP {price * quantity}</Card.Text>

    	            </Card.Body>
    	        </Card>
    	    </Col>
    	</Container>
       
    )
}*/