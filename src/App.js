import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom';
import {Routes, Route} from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Admin from './pages/Admin';
/*import AllCart from './pages/AllCart';*/
import AllProducts from './pages/AllProducts';
import AllUsers from './pages/AllUsers';
import ArchiveProduct from './pages/ArchiveProduct';
import Cart from './pages/Cart';
import CreateProducts from './pages/CreateProducts';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import PageNotFound from './pages/PageNotFound'
import Products from './pages/Products'
import ProductUpdate from './pages/ProductUpdate'
import ProductView from './pages/ProductView'
import ReactivateProduct from './pages/ReactivateProduct';
import Register from './pages/Register';
import Users from './pages/Users';
import './App.css';
import { UserProvider } from './UserContext';

function App() {

   const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

   const unsetUser = () => {
    localStorage.clear()
  }

  

  useEffect(() => {
    fetch('https://fathomless-spire-69300.herokuapp.com/users/details',{
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== "undefined") {

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })


      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }


    })


  }, [])


  return (
  <UserProvider value={{user, setUser, unsetUser}}>
    <Router> 
          <AppNavbar/>
          <Container>
          <Routes> 
              <Route path="/" element={<Home/>} />
              <Route path="/products" element={<Products/>} />
              <Route path="/products/:productId" element={<ProductView/>} />
              <Route path="/users" element={<Users/>} />
              <Route path="/users/mycart/" element={<Cart/>} />
              <Route path="/login" element={<Login/>} />
              <Route path="/logout" element={<Logout/>} />
              <Route path="/register" element={<Register/>} />
              <Route path ="*" element={<PageNotFound/>} />

              {
                (user.isAdmin === true) 

                ?
                <>
                <Route path="/admin" element={<Admin/>} />
                <Route path="/allusers" element={<AllUsers/>} />
                <Route path="/createproducts" element={<CreateProducts/>} />
                <Route path="/allproducts" element={<AllProducts/>} />
                <Route path="/products/update/:productId" element={<ProductUpdate/>} />
                <Route path="/products/:productId/archive" element={<ArchiveProduct/>} />
                <Route path="/products/:productId/reactivate" element={<ReactivateProduct/>} />
                
                
                </>
                :

                <>
                <Route path="/products" element={<Products/>} />
                

                </>
              }
              

          </Routes>
          </Container>
      </Router>
  </UserProvider>
  );
}

export default App;
